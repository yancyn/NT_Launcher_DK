package com.moan.nt_launcher_dk.launcher.window.view;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.moan.nt_launcher_dk.R;
import com.moan.nt_launcher_dk.launcher.window.manager.MyWindowManager;
import com.moan.nt_launcher_dk.others.server.FloatWindowService;
import com.moan.nt_launcher_dk.others.tools.fun.Fun;
import com.moan.nt_launcher_dk.others.tools.screen.ScreenUtils;


public class FloatWindowBigView extends LinearLayout {
    /**
     * 记录大悬浮窗的宽度
     */
    public static int viewWidth;
    /**
     * 记录大悬浮窗的高度
     */
    public static int viewHeight;

    /**
     * 当点击位置与悬浮框的距离小于该值时，不认为点击了空白区域
     */
    private static final int OFFSET_DISTANCE = 5;

    public FloatWindowBigView(final Context context) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.float_window_big, this);
        View view = findViewById(R.id.big_window_layout);
        viewWidth = view.getLayoutParams().width;
        viewHeight = view.getLayoutParams().height;
        Button system_outwindow = (Button) findViewById(R.id.system_outwindow);
        Button system_closewindow = (Button) findViewById(R.id.system_closewindow);
        Button system_allapp = (Button) findViewById(R.id.system_allapp);
        Button system_back = (Button) findViewById(R.id.system_back);
        Button system_push = (Button) findViewById(R.id.system_push);
        Button system_home = (Button) findViewById(R.id.system_home);
        Button system_menu = (Button) findViewById(R.id.system_menu);
        system_menu.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Fun.funMenu(context);
            }
        });
        system_home.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Fun.funHome(context);
            }
        });
        system_push.setText(MyWindowManager.getUsedPercentValue(context));
        system_push.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Fun.funBush(context);
            }
        });
        system_allapp.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Fun.funApp(context);
            }
        });
        system_back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Fun.funBack(context);
            }
        });
        system_closewindow.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // 点击返回的时候，移除大悬浮窗，创建小悬浮窗
                MyWindowManager.removeBigWindow(context);
                MyWindowManager.createSmallWindow(context);
            }
        });
        system_outwindow.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // 点击关闭悬浮窗的时候，移除所有悬浮窗，并停止Service
                MyWindowManager.removeBigWindow(context);
                MyWindowManager.removeSmallWindow(context);
                Intent intent = new Intent(getContext(), FloatWindowService.class);
                context.stopService(intent);
            }
        });


//        //打开应用
//        startApp.setOnClickListener(new OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(getContext(), MainActivity.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                getContext().startActivity(intent);
//
//                MyWindowManager.removeBigWindow(context);
//                MyWindowManager.createSmallWindow(context);
//            }
//        });
////        TextView percentView = (TextView) findViewById(R.id.percent);
////        percentView.setText();
//        close.setText(MyWindowManager.getUsedPercentValue(context));
//
//        //关闭悬浮框
//        close.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                // 点击关闭悬浮窗的时候，移除所有悬浮窗，并停止Service
//                MyWindowManager.removeBigWindow(context);
//                MyWindowManager.removeSmallWindow(context);
//                Intent intent = new Intent(getContext(), FloatWindowService.class);
//                context.stopService(intent);
//            }
//        });
//
//        //返回
//        back.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                // 点击返回的时候，移除大悬浮窗，创建小悬浮窗
//                MyWindowManager.removeBigWindow(context);
//                MyWindowManager.createSmallWindow(context);
//            }
//        });
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN &&
                isTouchBlank(event.getRawX(), event.getRawY())) {
            // 点击大悬浮框以外的区域，移除大悬浮窗，创建小悬浮窗
            MyWindowManager.removeBigWindow(getContext());
            MyWindowManager.createSmallWindow(getContext());
        }
        return true;
    }

    /**
     * 判断是否点击了大悬浮框以外的区域
     */
    private boolean isTouchBlank(float positionX, float positionY) {
        if (positionX < (ScreenUtils.getScreenW() - viewWidth) / 2 - OFFSET_DISTANCE ||
                positionX > ScreenUtils.getScreenW() / 2 + viewWidth / 2 + OFFSET_DISTANCE ||
                positionY < (ScreenUtils.getScreenH() - viewHeight) / 2 + ScreenUtils.getStatusBarHeight() - OFFSET_DISTANCE ||
                positionY > (ScreenUtils.getScreenH()) / 2 + viewHeight / 2 + ScreenUtils.getStatusBarHeight() + OFFSET_DISTANCE
        ) {
            return true;
        }
        return false;
    }
}
