package com.moan.nt_launcher_dk.launcher.settings.window;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


import androidx.appcompat.app.AppCompatActivity;

import com.moan.nt_launcher_dk.R;
import com.moan.nt_launcher_dk.others.permission.WindowPermission;
import com.moan.nt_launcher_dk.others.server.FloatWindowService;
import com.moan.nt_launcher_dk.others.tools.toast.DiyToast;


public class WindowSettingActivity extends AppCompatActivity {
    private TextView tv_title_text, tv_title_imagetext;
    private ImageView iv_title_back, iv_title_imagebutton;
    private Button btn_window_wuzhangai, btn_window_xuanfuchuang;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //全屏
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        requestWindowFeature(Window.FEATURE_NO_TITLE);// 无Title
        setContentView(R.layout.setting_window_setting);
        initView();
        tv_title_text.setText("悬浮球设置");
        iv_title_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        tv_title_imagetext.setText("检查权限");
        tv_title_imagetext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                create_dialog();
                /**
                 * 悬浮球服务
                 */
                Intent intent = new Intent(WindowSettingActivity.this, FloatWindowService.class);
                startService(intent);
            }
        });
        iv_title_imagebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                create_dialog();
                /**
                 * 悬浮球服务
                 */
                Intent intent = new Intent(WindowSettingActivity.this, FloatWindowService.class);
                startService(intent);
            }
        });
        btn_window_wuzhangai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS));
            }
        });
        btn_window_xuanfuchuang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent("android.settings.action.MANAGE_OVERLAY_PERMISSION");
                startActivity(intent);
            }
        });
    }

    private void create_dialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(WindowSettingActivity.this);
        builder.setTitle("悬浮窗权限");
        builder.setMessage("你即将进入权限申请界面\n请注意！申请后需要重启设备");
        builder.setPositiveButton("关闭", null);
        builder.setNeutralButton("给予权限", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                DiyToast.showToast(WindowSettingActivity.this, "请给予悬浮窗权限", false);
                //没有权限，需要申请权限，因为是打开一个授权页面，所以拿不到返回状态的，所以建议是在onResume方法中从新执行一次校验
                if (WindowPermission.check_window_permission(WindowSettingActivity.this)) {
                    DiyToast.showToast(WindowSettingActivity.this, "已经拥有悬浮窗权限", true);
                } else {
                    DiyToast.showToast(WindowSettingActivity.this, "请给予悬浮窗权限", true);
                    Intent intent = new Intent("android.settings.action.MANAGE_OVERLAY_PERMISSION");
                    startActivity(intent);
                }
            }
        });
        builder.setNegativeButton("关闭权限", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION);
                intent.setData(Uri.parse("package:" + getPackageName()));
                startActivityForResult(intent, 100);
            }
        });
        builder.show();
    }

    private void initView() {
        // TODO Auto-generated method stub
        btn_window_xuanfuchuang = (Button) findViewById(R.id.btn_window_xuanfuchuang);
        btn_window_wuzhangai = (Button) findViewById(R.id.btn_window_wuzhangai);
        tv_title_text = (TextView) findViewById(R.id.tv_title_text);
        iv_title_back = (ImageView) findViewById(R.id.iv_title_back);
        tv_title_imagetext = (TextView) findViewById(R.id.tv_title_imagetext);
        iv_title_imagebutton = (ImageView) findViewById(R.id.iv_title_imagebutton);
    }
}