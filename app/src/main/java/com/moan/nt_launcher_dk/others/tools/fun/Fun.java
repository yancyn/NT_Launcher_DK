package com.moan.nt_launcher_dk.others.tools.fun;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;

import com.moan.nt_launcher_dk.launcher.window.manager.MyWindowManager;

public class Fun {

    public static void funApp(final Context context) {
        Intent intent = new Intent();
        intent.setComponent(new ComponentName("com.mgs.settings", "com.mgs.settings.app.AppMain"));
        context.startActivity(intent);
    }

    public static void funBack(final Context context) {
        Intent intent = new Intent("com.mogu.back_key");
        context.sendBroadcast(intent);
    }

    public static void funBush(final Context context) {
        Intent intent = new Intent("com.mogu.clear_mem");
        context.sendBroadcast(intent);
    }

    public static void funHome(final Context context) {
        Intent intent = new Intent();// 创建Intent对象
        intent.setAction(Intent.ACTION_MAIN);// 设置Intent动作
        intent.addCategory(Intent.CATEGORY_HOME);// 设置Intent种类
        context.startActivity(intent);
    }

    public static void funMenu(final Context context) {
        MyWindowManager.removeBigWindow(context);
        MyWindowManager.createSmallWindow(context);
    }

}
