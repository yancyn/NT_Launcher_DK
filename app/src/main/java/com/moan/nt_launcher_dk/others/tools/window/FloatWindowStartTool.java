package com.moan.nt_launcher_dk.others.tools.window;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.moan.nt_launcher_dk.others.server.FloatWindowService;
import com.moan.nt_launcher_dk.others.tools.screen.ScreenUtils;

public class FloatWindowStartTool {
    private static String TAG = "FloatWindowStartTool";

    public static void start(final Context context, final Activity activity) {
        ScreenUtils.initScreen(activity);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.canDrawOverlays(context.getApplicationContext())) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("权限申请，用于显示悬浮球");
                builder.setMessage("请给予  悬浮窗  权限，当您点击确定并给予  悬浮窗  权限后，您需要重启本软件或重启设备");
                builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //启动Activity让用户授权
                        Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION);
                        intent.setData(Uri.parse("package:" + context.getPackageName()));
                        activity.startActivityForResult(intent, 100);
                    }
                });
                builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                builder.show();
            } else {
                context.startService(new Intent(context, FloatWindowService.class));
//                        if (isAccessibilitySettingsOn(MainActivity.this)) {
//
//                        } else {
//                            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
//                            builder.setTitle("权限申请（2/2），用于返回功能");
//                            builder.setMessage("请给予  无障碍  权限，当您点击确定并给予  无障碍  权限后，您需要重启本软件或重启设备");
//                            builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
//                                    startActivity(new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS));
//                                }
//                            });
//                            builder.setNegativeButton("取消", null);
//                            builder.show();
//                        }
                //finish();
            }
        }
    }
}
