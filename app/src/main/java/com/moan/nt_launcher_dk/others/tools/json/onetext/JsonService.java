/**
 * Copyright 2020 bejson.com
 */
package com.moan.nt_launcher_dk.others.tools.json.onetext;

/**
 * Auto-generated: 2020-02-14 15:37:24
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class JsonService {

    /**
     * id : 4671
     * hitokoto : 我不借助别人的力量就没办法跟仰慕的人聊天吗？
     * type : a
     * from : 女高中生的虚度日常
     * from_who : null
     * creator : eaea
     * creator_uid : 304
     * reviewer : 1044
     * uuid : 508bd372-d72e-4af8-ab4d-7bb77adec6a9
     * created_at : 1569409493
     */

    private int id;
    private String hitokoto;
    private String type;
    private String from;
    private Object from_who;
    private String creator;
    private int creator_uid;
    private int reviewer;
    private String uuid;
    private String created_at;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHitokoto() {
        return hitokoto;
    }

    public void setHitokoto(String hitokoto) {
        this.hitokoto = hitokoto;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public Object getFrom_who() {
        return from_who;
    }

    public void setFrom_who(Object from_who) {
        this.from_who = from_who;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public int getCreator_uid() {
        return creator_uid;
    }

    public void setCreator_uid(int creator_uid) {
        this.creator_uid = creator_uid;
    }

    public int getReviewer() {
        return reviewer;
    }

    public void setReviewer(int reviewer) {
        this.reviewer = reviewer;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
}