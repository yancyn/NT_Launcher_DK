package com.moan.nt_launcher_dk.others.permission;

import android.content.Context;
import android.os.Build;
import android.provider.Settings;

public class WindowPermission {
    public static boolean check_window_permission(Context context) {
        boolean hasPermission = false;
        if (Build.VERSION.SDK_INT >= 23) {
            if (Settings.canDrawOverlays(context)) {
                hasPermission = true;
            } else {
                hasPermission = false;
            }
        } else {
            hasPermission = true;
        }
        return hasPermission;
    }
}
